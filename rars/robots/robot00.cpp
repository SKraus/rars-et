/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"
#include <math.h>
#include <stdlib.h>


//--------------------------------------------------------------------------
//                           Class Robot00
//--------------------------------------------------------------------------

class Robot00 : public Driver
    {
      public:
                Robot00()
                    {
                        m_sName = "Test";
                        m_sAuthor = "Lucky Luke";
                        m_iNoseColor = oBLUE;
                        m_iTailColor = oBLUE;
                        m_sBitmapName2D = "car_blue_blue";
                        m_sModel3D = "futura";
                    }

                con_vec drive(situation& s)
                    {

                        double breite=s.to_lft+s.to_rgt;
                        double laenge=s.cur_len;
                        double laengezumende=s.to_end;
                        double links=s.to_lft;
                        double rechts=s.to_rgt;
                        double lenkfaktor1 =0.1;
                        double lenkfaktor2 =0.05;
                        double lenkfaktor3 =0.1;
                        double lenkfaktor4 =0.05;
                        double aklenkwinkel=s.vn;
                        double akgeschwindigkeit=s.v;
                        double radius= s.cur_rad;
                        double abweichung=s.to_lft-s.to_rgt;
                        double nextrad=fabs(s.nex_rad);
                        double radabs=fabs(radius);
                        con_vec result = CON_VEC_EMPTY;

                        if( s.starting )
                            {
                                result.fuel_amount = MAX_FUEL; // fuel when starting

                            }
                        if (radius==0)
                        {
                            if (fabs(abweichung)<2)
                                result.alpha=-s.vn;
                            else
                                result.alpha=((links-rechts)/breite)*lenkfaktor1-(aklenkwinkel/akgeschwindigkeit)*lenkfaktor2;
                            if ((laenge/laengezumende)>8)
                                result.vc=nextrad*0.05/akgeschwindigkeit;

                            else
                            {
                                result.vc =200;
                            }
                        }

                        else
                        {


                                    result.vc=3*radabs/akgeschwindigkeit;


                                if ((laenge/laengezumende)<2)
                                {

                                        if  ((laenge/laengezumende)<1.2)
                                        {
                                            result.alpha=((links-rechts)/breite)*0.7-(aklenkwinkel/akgeschwindigkeit)*0.5;

                                        }
                                        else
                                        {
                                            result.alpha=((((laenge/laengezumende)*links-rechts))/breite)*lenkfaktor1-(aklenkwinkel/akgeschwindigkeit)*lenkfaktor2;
                                        }
                                }
                                else
                                {
                                    result.alpha=((((1-(laengezumende/laenge))*links-rechts))/breite)*lenkfaktor3-(aklenkwinkel/akgeschwindigkeit)*lenkfaktor4;
                                }

                                if (nextrad<radabs && nextrad!=0)
                                {

                                    if ((laenge/laengezumende)>8)
                                        result.vc=nextrad*0.5/akgeschwindigkeit;


                                }

                        }

                        //result.vc = 50; // going slowly
                        result.request_pit = 0; // do not ask to pit
                        return result;

                    }
        };


/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver* getRobot00Instance()
{
    return new Robot00();
}
