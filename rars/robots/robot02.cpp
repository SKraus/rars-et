/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"
#include <cmath>
#include <cstdlib>

//--------------------------------------------------------------------------
//                           Class Robot02
//--------------------------------------------------------------------------

class Robot02 : public Driver
{
public:
    // Konstruktor
    Robot02()
    {
        // Der Name des Robots
        m_sName = "Robot02";
        // Namen der Autoren
        m_sAuthor = "";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLUE;
        m_iTailColor = oBLUE;
        m_sBitmapName2D = "car_blue_blue";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

    con_vec drive(situation& s)
    {
        double winkel1;
        const double winkelfunktion = 0.0000011;
        // hier wird die Logik des Robots implementiert
        con_vec result = CON_VEC_EMPTY;
        if( s.starting )
                            {
                                result.fuel_amount = MAX_FUEL; // fuel when starting

                            }
        winkel1=abs(s.cur_rad-180);
        if (s.cur_rad<0)
            result.alpha=-winkel1*winkelfunktion;
        else if (s.cur_rad>0)
            result.alpha=winkel1*winkelfunktion;
        else
            result.alpha=0.0;

        if (s.to_lft<10)
            result.alpha=-0.01;
        if (s.to_rgt<10)
            result.alpha=0.01;

        /*if (s.to_lft > STRECKEHALBE )
        {
            result.alpha= 0.005;
        }
        if (s.to_lft < STRECKEHALBE )
        {
            result.alpha= -0.005;
        }

        else
            result.alpha=0.0;*/
        /*if (abs(s.cur_rad)>=100)
            result.vc=5;
        if (abs(s.cur_rad)<100)
            result.vc=5;
        if (abs(s.cur_rad)<80)
            result.vc=7;
        if (abs(s.cur_rad)<60)
            result.vc=10;
        if (abs(s.cur_rad)<40)
            result.vc=20;
        if (abs(s.cur_rad)<20)
            result.vc=30;
        else*/
            result.vc=20;


        return result;
    }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot02Instance()
{
    return new Robot02();
}
